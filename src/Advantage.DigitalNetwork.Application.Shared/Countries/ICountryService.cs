﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Advantage.DigitalNetwork.Countries.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Advantage.DigitalNetwork.Countries
{
   public interface ICountryService: IApplicationService
    {
        Task<CountryDto> CreateCountry(CountryRequestInput input);
        Task<CountryDto> UpdateCountry(CountryRequestInput input);
        Task DeleteCountry(int Id);
        //Task<CountryResponseOutput> GetCountry();
        ListResultDto<CountryDto> GetCountry();
    }
}
