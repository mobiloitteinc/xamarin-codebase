﻿using Advantage.DigitalNetwork.Dto;

namespace Advantage.DigitalNetwork.Common.Dto
{
    public class FindUsersInput : PagedAndFilteredInputDto
    {
        public int? TenantId { get; set; }
    }
}