﻿using System.Collections.Generic;

namespace Advantage.DigitalNetwork.Chat.Dto
{
    public class ChatUserWithMessagesDto : ChatUserDto
    {
        public List<ChatMessageDto> Messages { get; set; }
    }
}