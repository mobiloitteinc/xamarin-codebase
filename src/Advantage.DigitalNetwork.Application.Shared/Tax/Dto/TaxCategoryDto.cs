﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Advantage.DigitalNetwork.Tax.Dto
{
    public class TaxCategoryDto : EntityDto
    {
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
