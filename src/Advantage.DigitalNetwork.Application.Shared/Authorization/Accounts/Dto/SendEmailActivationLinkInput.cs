﻿using System.ComponentModel.DataAnnotations;

namespace Advantage.DigitalNetwork.Authorization.Accounts.Dto
{
    public class SendEmailActivationLinkInput
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}