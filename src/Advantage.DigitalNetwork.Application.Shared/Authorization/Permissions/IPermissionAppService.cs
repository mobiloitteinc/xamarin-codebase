﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Advantage.DigitalNetwork.Authorization.Permissions.Dto;

namespace Advantage.DigitalNetwork.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
