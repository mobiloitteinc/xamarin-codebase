﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Advantage.DigitalNetwork.Configuration.Host.Dto;

namespace Advantage.DigitalNetwork.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
