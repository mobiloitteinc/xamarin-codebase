﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Advantage.DigitalNetwork.Configuration.Tenants.Dto;

namespace Advantage.DigitalNetwork.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
